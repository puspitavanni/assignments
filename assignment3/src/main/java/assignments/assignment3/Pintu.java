package assignments.assignment3;

public class Pintu extends Benda {
    
    /**
     * Mengimplementasikan abstract method yang terdapat pada class Benda.
     */
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular() + 30);
        if (this.getPersentaseMenular() >= 100) {
            this.ubahStatus("positif");	
        }
    }
    
    /**
     * Membuat Constructor dari class Pintu.
     * Mengakses constructor superclass-nya.
     * @param nama
     */
    public Pintu(String nama) {
        super(nama, "PINTU");
    }

    
    /**
     * Method toString yang di-override dari superclass-nya.
     */
    @Override
    public String toString() {
        return String.format("PINTU %s", getNama());
    }
}