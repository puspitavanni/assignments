package assignments.assignment3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class CleaningService extends Manusia {
    	
    private int jumlahDibersihkan;

    
    /**
     * Constructor untuk CleaningService.
     * Mengakses constructor superclass-nya.
     * @param nama
     */
    public CleaningService(String nama) {
        super(nama, "CLEANING_SERVICE");
    }

    
    /**
     * Method bersihkan untuk benda.
     * Berfungsi untuk me-set persentaseMenular dari suatu benda menjadi 0 kembali.
     * Membuat statusCovid dari benda yang telah positif menjadi negatif kembali.
     * Mengurangi aktifKasusDisebabkan.
     * @param benda
     */
    public void bersihkan(Benda benda) {
        if (this instanceof CleaningService && benda instanceof Benda) {
            if (benda.getPersentaseMenular() > 0 && benda.getPersentaseMenular() < 100) {
                benda.setPersentaseMenular(0);
                jumlahDibersihkan++;
            } else {
                benda.setPersentaseMenular(0);
                benda.ubahStatus("negatif");
                jumlahDibersihkan++;
                this.kurangi(benda);
            }   
        }
    }

    
    /**
     * Mengembalikan nilai dari atribut jumlahDibersihkan.
     * @return
     */
    public int getJumlahDibersihkan() {
        return jumlahDibersihkan;
    }

    
    /**
     * Method untuk mengembalikan nilai String dari objek.
     */
    @Override
    public String toString() {
        return String.format("CLEANING SERVICE %s", getNama());
    }

}