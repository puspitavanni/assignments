package assignments.assignment3;

public class TombolLift extends Benda {

    /**
     * Mengimplementasikan abstract method yang terdapat pada class Benda.
     * Menambahkan persentaseMenular.
     */
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular() + 20);
        if (this.getPersentaseMenular() >= 100) {
            this.ubahStatus("positif");
        }
    }
      
    /**
     * Membuat constructor untuk TombolLift.
     * Mengakses constructor superclass-nya.
     * @param nama
     */
    public TombolLift(String nama) {
        super(nama, "TOMBOL_LIFT");
    }

    @Override
    /**
     * Method toString yang di-override dari superclass-nya.
     */
    public String toString() {
        return String.format("TOMBOL LIFT %s", getNama());
    }
}