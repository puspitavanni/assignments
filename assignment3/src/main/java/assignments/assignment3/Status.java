package assignments.assignment3;

public interface Status {
    /**
     * Membuat abstract method dari class interface Status.
     * @return
     */
    public abstract String getStatus();
    public abstract void tularkan(Carrier penular, Carrier tertular);
}