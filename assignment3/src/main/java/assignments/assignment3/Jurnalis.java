package assignments.assignment3;

public class Jurnalis extends Manusia {
      
    /**
     * Membuat constructor untuk Jurnalis.
     * Mengakses constructor superclass-nya.
     * @param nama
     */
    public Jurnalis(String nama) {
        super(nama, "JURNALIS");
    }

    
    /**
     * Mengakses constructor superclass-nya.
     */
    @Override
    public String toString() {
        return String.format("JURNALIS %s", getNama());
    }

}