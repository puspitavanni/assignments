package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World {

    public List<Carrier> listCarrier;

    /**
     * Membuat Constructor untuk class World.
     */
    public World() {
        this.listCarrier =  new ArrayList<Carrier>();
    }

    /**
     * Mengimplementasikan apabila ingin membuat objek sesuai dengan parameter yang diberikan.
     * @param tipe
     * @param nama
     * @return
     */
    public Carrier createObject(String tipe, String nama) {
        if (tipe.equals("ANGKUTAN_UMUM")) {
            Carrier angkutanUmum = new AngkutanUmum(nama);
            listCarrier.add(angkutanUmum);
            return angkutanUmum;
        } else if (tipe.equals("CLEANING_SERVICE")) {
            Carrier cleaningService = new CleaningService(nama);
            listCarrier.add(cleaningService);
            return cleaningService;
        } else if (tipe.equals("PEGANGAN_TANGGA")) {
            Carrier peganganTangga = new PeganganTangga(nama);
            listCarrier.add(peganganTangga);
            return peganganTangga;
        } else if (tipe.equals("PINTU")) {
            Carrier pintu = new Pintu(nama);
            listCarrier.add(pintu);
            return pintu;
        } else if (tipe.equals("TOMBOL_LIFT")) {
            Carrier tombolLift = new TombolLift(nama);
            listCarrier.add(tombolLift);
            return tombolLift;
        } else if (tipe.equals("JURNALIS")) {
            Carrier jurnalis = new Jurnalis(nama);
            listCarrier.add(jurnalis);
            return jurnalis;
        } else if (tipe.equals("OJOL")) {
            Carrier ojol = new Ojol(nama);
            listCarrier.add(ojol);
            return ojol;
        } else if (tipe.equals("PEKERJA_JASA")) {
            Carrier pekerjaJasa = new PekerjaJasa(nama);
            listCarrier.add(pekerjaJasa);
            return pekerjaJasa;
        } else {
            Carrier petugasMedis = new PetugasMedis(nama);
            listCarrier.add(petugasMedis);
            return petugasMedis;
        }
    }

    /**
     * Mengimplementasikan apabila ingin mengambil objek carrier dengan nama sesuai dengan parameter.
     * @param nama
     * @return
     */
    public Carrier getCarrier(String nama) {
        Carrier cariCarrier = null;
        for (Carrier findCarrier : listCarrier) {
            if (findCarrier.getNama().equalsIgnoreCase(nama)) {
                cariCarrier = findCarrier;
            } 
        } 
        return cariCarrier;
    }
}
