package assignments.assignment3;

public abstract class Manusia extends Carrier {
    private static int jumlahSembuh = 0;
    
    /**
     * Membuat constructor untuk Manusia.
     * Mengakses constructor superclass-nya.
     * @param nama
     * @param tipe
     */
    public Manusia(String nama, String tipe) {
        super(nama, tipe);
    }
    
    /**
     * Fungsi untuk menambahkan nilai pada atribut jumlahSembuh.
     * Akses modifiernya bersifat public yang berarti dapat diakses di semua class.
     */
    public void tambahSembuh() {
        jumlahSembuh++;
    }

    /**
     * Mengembalikan nilai untuk atribut jumlahSembuh.
     * @return
     */
    public static int getJumlahSembuh() {
        return jumlahSembuh;
    }
    
}