package assignments.assignment3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public abstract class Carrier {

    private String nama;
    private String tipe;
    private Status statusCovid;
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular;
  

    /**
     * Membuat Constructor untuk class Carrier.
     * @param nama
     * @param tipe
     */
    public Carrier(String nama,String tipe) {
        this.nama = nama;
        this.tipe = tipe;
        this.rantaiPenular = new ArrayList<Carrier>();
        this.statusCovid = new Negatif();
    }
    

    /**
     * Mengembalikan nilai dari atribut nama.
     * @return
     */
    public String getNama() {
        return this.nama;
    }
    

    /**
     * Mengembalikan nilai dari atribut tipe.
     * @return
     */
    public String getTipe() {
        return this.tipe;
    }
    

    /**
     * Mengembalikan nilai dari atribut statusCovid.
     * @return
     */
    public String getStatusCovid() {
        return this.statusCovid.getStatus();
    }
    

    /**
     * Mengembalikan nilai dari atribut aktifKasusDisebabkan.
     * @return
     */
    public int getAktifKasusDisebabkan() {
        return this.aktifKasusDisebabkan;
    }
    

    /**
     * Mengembalikan nilai atribut totalKasusDisebabkan.
     * @return
     */
    public int getTotalKasusDisebabkan() {
        return this.totalKasusDisebabkan;
    }
    

    /**
     * Mengembalikan nilai dari atribut rantaiPenular.
     * @return
     */
    public List<Carrier> getRantaiPenular() throws BelumTertularException {
        if (this.getStatusCovid().equalsIgnoreCase("negatif")) {
            throw new BelumTertularException(this + " berstatus negatif");
    } 
        return this.rantaiPenular;
    }

    
    /**
     * Mengembalikan rantaiPenular.
     * @param listRantaiPenular
     */
    public void setRantaiPenular(List<Carrier> listRantaiPenular) {
        this.rantaiPenular = new ArrayList<Carrier>(listRantaiPenular);
    }

    /**
     * Fungsi ini untuk mengubah atribut dari statusCovid
     * @param status
     */
    public void ubahStatus(String status) {
        if (status.equalsIgnoreCase("positif")) {
            this.statusCovid = new Positif();
        } else if (status.equalsIgnoreCase("negatif")) {
            this.statusCovid = new Negatif();
        }
    }

    /**
     * Objek ini berinteraksi dengan objek lain.
     * @param lain
     */
    public void interaksi(Carrier lain) {
        this.getStatusCovid();
        lain.getStatusCovid();
        if (this.getStatusCovid().equalsIgnoreCase("positif") && lain.getStatusCovid().equalsIgnoreCase("negatif")) {
            this.statusCovid.tularkan(this, lain);
        } else if (this.getStatusCovid().equalsIgnoreCase("negatif") && lain.getStatusCovid().equalsIgnoreCase("positif")) {
            lain.statusCovid.tularkan(lain, this);
        }
    }
    
    
    /**
     * Method baru yang bernama tambahan.
     * Untuk menambahkan aktifKasusDisebabkan dan totalKasusDisebabkan.
     * @param tertular
     */
    public void tambahan(Carrier tertular) {
       tertular.rantaiPenular = new ArrayList<Carrier>(this.rantaiPenular);      
       tertular.rantaiPenular.add(tertular);
       
       HashSet<Carrier> semuaPenular = new HashSet<>();
       for(int i = 0; i < tertular.rantaiPenular.size()-1; i++) {
        	semuaPenular.add(tertular.rantaiPenular.get(i));
        } 

        for (Carrier elem : semuaPenular) {
            elem.aktifKasusDisebabkan++;
            elem.totalKasusDisebabkan++;
        } 
    } 
    
    
    /**
     * Method baru yang bernama kurangi.
     * Untuk mengurangi aktifKasusDisebabkan.
     * @param objek
     */
    public void kurangi(Carrier objek) {
    	HashSet<Carrier> semuaPenular = new HashSet<Carrier>();
    	
        for(int i = 0; i < objek.rantaiPenular.size()-1; i++) {
            semuaPenular.add(objek.rantaiPenular.get(i));
        } 
        for (Carrier c : semuaPenular) {
            c.aktifKasusDisebabkan--;
        } 
    }
    
    
    /**
     * Method baru untuk menambahkan objek itu sendiri ke rantaiPenular.
     * Method ini digunakan saat query POSITIFKAN.
     */
    public void tambahRantaiSendiri() {
        this.rantaiPenular.add(this);
    }
    
    
    /**
     * Ini method abstract dari abstract class Carrier.
     */
    public abstract String toString();
  
}
