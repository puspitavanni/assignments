package assignments.assignment3;

public class PekerjaJasa extends Manusia {
      
    /**
     * Membuat constructor untuk PekerjaJasa.
     * Mengakses constructor superclass-nya.
     * @param nama
     */
    public PekerjaJasa(String nama) {
        super(nama, "PEKERJA_JASA");
    }
    
    @Override
    /**
     * Method toString yang di-override dari superclass-nya.
     */
    public String toString() {
        return String.format("PEKERJA JASA %s", getNama());
    }
}