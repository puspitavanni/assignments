package assignments.assignment3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Positif implements Status {
  
    /**
     * Mengembalikan nilai dari status.
     */
    public String getStatus() {
        return "positif";
    }

    /**
     * Mengimplementasikan apabila object Penular melakukan interaksi dengan object tertular.
     * Menghandle kasus ketika keduanya benda dapat dilakukan disini.
     */
    public void tularkan(Carrier penular, Carrier tertular) {
        if (tertular instanceof Manusia) {
            tertular.ubahStatus("positif");
           penular.tambahan(tertular);
        } else if (penular instanceof Manusia && tertular instanceof Benda) {
            Benda b = (Benda)tertular;
            b.tambahPersentase();
            if (b.getStatusCovid().equalsIgnoreCase("positif")) {
            penular.tambahan(b);
            }
        } else {
            System.out.println("TIDAK BISA");
        }
    }
}

