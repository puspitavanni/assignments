package assignments.assignment3;

public class AngkutanUmum extends Benda {

    /**
     * Membuat method tambahPersentase() untuk menambahkan sesuai persentase benda.
     * Mengimplementasikan abstract method yang terdapat pada class Benda.
     */
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular() + 35);
        if (this.getPersentaseMenular() >= 100) {
            this.ubahStatus("positif");
        }
    }

    /**
     * Membuat constructor untuk AngkutanUmum.
     * Mengakses constructor superclass-nya.
     * param(nama, tipe).
     */
    public AngkutanUmum(String nama) {
        super(nama, "ANGKUTAN_UMUM");
    }

    /**
     * Method toString yang di-override dari superclass nya.
     */
    @Override
    public String toString() {
        return String.format("ANGKUTAN UMUM %s", getNama());
    }
}