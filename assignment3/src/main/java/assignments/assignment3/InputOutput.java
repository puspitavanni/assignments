package assignments.assignment3;


import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InputOutput {

    private BufferedReader br;
    private PrintWriter pw;
    private String inputFile;
    private String outputFile; 
    private World world;

    /**
     * Membuat Constructor untuk InputOutput.
     * @param inputType
     * @param inputFile
     * @param outputType
     * @param outputFile
     */
    public InputOutput(String inputType, String inputFile, String outputType, String outputFile) {
        if (inputType.equalsIgnoreCase("text")) {
            this.inputFile = inputFile;
        }
        if (outputType.equalsIgnoreCase("text")) {
            this.outputFile = outputFile;
        }
        this.setPrintWriter(outputType);
        this.setBufferedReader(inputType);
    }

    /**
     * Membuat BufferedReader bergantung inputType (I/O text atau input terminal). 
     * @param inputType
     */
    public void setBufferedReader(String inputType) {
        if (inputType.equalsIgnoreCase("text")) {
            try {
                br = new BufferedReader(new FileReader(inputFile));
            } catch (FileNotFoundException e) {
                e.toString();
            }
        } else if (inputType.equalsIgnoreCase("terminal")) {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
    }
    

    /**
     * Membuat PrintWriter bergantung inputType (I/O text atau output terminal). 
     * @param outputType
     */
    public void setPrintWriter(String outputType) {
        if (outputType.equalsIgnoreCase("text")) {
            try {
                pw = new PrintWriter(outputFile);
            } catch (FileNotFoundException e) {
                e.toString();
            }
        } else if (outputType.equalsIgnoreCase("terminal")) {
            pw = new PrintWriter(System.out);
        }
    }

    
    /**
     * Program utama untuk InputOutput.
     * Memanfaatkan method simulateTerminal jika inputType yang dimasukkan adalah "TERMINAL".
     * @throws IOException
     */
    public void run() throws IOException {
        World world = new World();
        boolean kondisi = true;
        while (kondisi) {
        String line = br.readLine();
        String[] query = line.split(" ");
            if (query[0].equalsIgnoreCase("ADD")) {
                world.createObject(query[1], query[2]);
            } else if (query[0].equalsIgnoreCase("INTERAKSI")) {
                String objek_satu = query[1];
                String objek_dua = query[2];
                Carrier a = world.getCarrier(objek_satu);
                Carrier b = world.getCarrier(objek_dua);
                if (a instanceof Carrier && b instanceof Carrier) {
                    a.interaksi(b);
                } else {
                    pw.println("Objek yang dimaksud belum ditambahkan");
                }
            } else if (query[0].equalsIgnoreCase("POSITIFKAN")) {   
                String nama = query[1];
                Carrier objek = world.getCarrier(nama);
                if (objek instanceof Carrier) {
                    objek.ubahStatus("positif");
                    objek.tambahRantaiSendiri();
                } else {
                    pw.println("Objek yang dimaksud belum ditambahkan");
                }
            } else if (query[0].equalsIgnoreCase("SEMBUHKAN")) {
                String objek_satu = query[1];
                String objek_dua = query[2];
                PetugasMedis a = (PetugasMedis)world.getCarrier(objek_satu);
                Manusia b = (Manusia)world.getCarrier(objek_dua);
                a.obati(b);
            } else if (query[0].equalsIgnoreCase("BERSIHKAN")) {
                String objek_satu = query[1];
                String objek_dua = query[2];
                CleaningService a = (CleaningService)world.getCarrier(objek_satu);
                Benda b = (Benda)world.getCarrier(objek_dua);
                if (a instanceof CleaningService && b instanceof Benda) {
                    a.bersihkan(b);
                } else {
                    pw.println("Objek yang dimaksud belum ditambahkan");
                }
            } else if (query[0].equalsIgnoreCase("RANTAI")) {
                String nama = query[1];
                Carrier objek = world.getCarrier(nama);
                try { 
                    String s = "Rantai penyebaran " + objek + ": ";
                    for (int i = 0; i < objek.getRantaiPenular().size(); i++) {
                        Carrier a = objek.getRantaiPenular().get(i);
                        s += (a + " ");
                        if (i == objek.getRantaiPenular().size()-1) {
                        } else {
                            s += "-> ";
                        }
	                } 
                    pw.println(s);
                } catch (BelumTertularException ex) {
                    pw.println(ex);
                }  
            } else if (query[0].equalsIgnoreCase("TOTAL_KASUS_DARI_OBJEK")) {
                String nama = query[1];
                Carrier objek = world.getCarrier(nama);
                pw.println(objek + " telah menyebarkan virus COVID ke " + objek.getTotalKasusDisebabkan() + " objek");
            } else if (query[0].equalsIgnoreCase("AKTIF_KASUS_DARI_OBJEK")) {
                String nama = query[1];
                Carrier objek = world.getCarrier(nama);
                pw.println(objek + " telah menyebarkan virus COVID dan masih teridentifikasi positif sebanyak " 
                    + objek.getAktifKasusDisebabkan() + " objek");
            } else if (query[0].equalsIgnoreCase("TOTAL_SEMBUH_MANUSIA")) {
                pw.println("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: " + Manusia.getJumlahSembuh() + " kasus");
            } else if (query[0].equalsIgnoreCase("TOTAL_SEMBUH_PETUGAS_MEDIS")) {
                String nama = query[1];
                PetugasMedis objek = (PetugasMedis)world.getCarrier(nama);
                pw.println(objek.toString() + " menyembuhkan " + objek.getJumlahDisembuhkan() + " manusia");
            } else if (query[0].equalsIgnoreCase("TOTAL_BERSIH_CLEANING_SERVICE")) {
                String nama = query[1];
                CleaningService objek = (CleaningService)world.getCarrier(nama);
                pw.println(objek.toString() + " membersihkan " + objek.getJumlahDibersihkan() + " benda");
            } else if (query[0].equalsIgnoreCase("EXIT")) { 
            	pw.flush();
            	pw.close();
                kondisi = false;
                System.out.println("----------------------------------oOo-----------------------------------");
                
            }
        }
    }
}
       


    
