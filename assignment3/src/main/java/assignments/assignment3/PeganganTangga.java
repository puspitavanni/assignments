package assignments.assignment3;

public class PeganganTangga extends Benda {

    /**
     * Mengimplementasikan abstract method yang terdapat pada class Benda.
     */
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular() + 20);
        if (this.getPersentaseMenular() >= 100) {
            this.ubahStatus("positif");
        }
    }
      
    /**
     * Membuat constructor untuk PeganganTangga.
     * Mengakses constructor superclass-nya.
     * @param nama
     */
    public PeganganTangga(String nama) {
        super(nama, "PEGANGAN_TANGGA");
    }

    
    /**
     * Method toString yang di-override dari superclass-nya.
     */
    @Override
    public String toString() {
        return String.format("PEGANGAN TANGGA %s", getNama());
    }

}