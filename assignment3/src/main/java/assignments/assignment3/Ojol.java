package assignments.assignment3;

public class Ojol extends Manusia {
      
    /**
     * Membuat Constructor untuk Ojol.
     * Mengakses constructor superclass-nya.
     * @param nama
     */
    public Ojol(String nama) {
        super(nama, "OJOL");
    }

   
    /**
     * Method toString yang di-override dari superclass-nya.
     */
    @Override
    public String toString() {
        return String.format("OJOL %s", getNama());
    }
}