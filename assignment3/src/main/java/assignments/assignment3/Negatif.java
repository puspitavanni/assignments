package assignments.assignment3;

public class Negatif implements Status {
  
    /**
     * Mengembalikan nilai dari status.
     */
    public String getStatus() {
        return "negatif";
    }

    /**
     * Meng-override method tularkan yang ada di class Status.
     * ((Tidak melakukan apapun)).
     */
    public void tularkan(Carrier penular, Carrier tertular) {
        return;
    }
}