package assignments.assignment3;

public class BelumTertularException extends Exception {

    /**
    * Constructor dari BelumTertularException.
    * param(errorMessage).
    */
    public BelumTertularException(String errorMessage) {
        super(errorMessage);
    }
}