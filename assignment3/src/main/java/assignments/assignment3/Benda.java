package assignments.assignment3;

public abstract class Benda extends Carrier {
  
    protected int persentaseMenular;

    /**
     * Constructor dari Benda.
     * Mengakses constructor superclass-nya
     * param(nama, tipe).
     */
    public Benda(String nama, String tipe) {
        super(nama, tipe);
    }

    
    /**
     * Abstract method dari abstract class Benda.
     */
    public abstract void tambahPersentase();

    
    /**
     * Mengembalikan nilai dari persentaseMenular.
     */
    public int getPersentaseMenular() {
        return this.persentaseMenular;
    }
    
    
    /**
     * Untuk men-set nilai dari persentaseMenular.
     */
    public void setPersentaseMenular(int persentase) {
        this.persentaseMenular = persentase;
    }
}