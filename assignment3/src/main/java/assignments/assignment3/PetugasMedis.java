package assignments.assignment3;

import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;

public class PetugasMedis extends Manusia {
            	
    private int jumlahDisembuhkan;

    /**
     * Membuat constructor untuk Jurnalis.
     * Mengakses constructor superclass-nya.
     * @param nama
     */
    public PetugasMedis(String nama) {
        super(nama, "PETUGAS_MEDIS");
    }

    /**
     * Mengimplementasikan apabila objek PetugasMedis ini menyembuhkan manusia.
     * @param manusia
     */
    public void obati(Manusia manusia) {
        if (this instanceof PetugasMedis && manusia.getStatusCovid().equals("positif")) {
            manusia.ubahStatus("negatif");
            manusia.tambahSembuh();
            jumlahDisembuhkan++;
            this.kurangi(manusia);       
        }     	
    } 
     
    /**
     * Mengembalikan nilai dari atribut jumlahDisembuhkan.
     * @return
     */
    public int getJumlahDisembuhkan() {
        return this.jumlahDisembuhkan;
    }

    
    @Override
    /**
     * Method toString yang di-override dari superclass-nya.
     */
    public String toString() {
        return String.format("PETUGAS MEDIS %s", getNama());
    }
}