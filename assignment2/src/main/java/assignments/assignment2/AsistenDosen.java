package assignments.assignment2;

import java.util.*;
import java.lang.*;

public class AsistenDosen {
    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    /**
     * AsistenDosen.
     */
    public AsistenDosen(String kode, String nama) {
        // Constructor untuk AsistenDosen.
        this.kode = kode;
        this.nama = nama;
    }

    /**
     * getKode().
     */
    public String getKode() {
        // Mengembalikan kode AsistenDosen.
        return this.kode;
    }

    /**
     * addMahasiswa(Mahasiswa mhs).
     */
    public void addMahasiswa(Mahasiswa mhs) {
        // Menambahkan mahasiswa ke dalam daftar mahasiswa dengan mempertahankan urutan.
        // Hint: ((kamu boleh menggunakan Collections.sort atau melakukan sorting manual)).
        // Memanfaatkan method compareTo pada Mahasiswa.
        this.mahasiswa.add(mhs);
        Collections.sort(this.mahasiswa, new SortByNPM());
    }
    
    public void hapusMahasiswa(Mahasiswa mhs) {
        // Menghapus data mahasiswa yang ingin diganti nama AsDos nya
        this.mahasiswa.remove(mhs);
    }

    /**
     * getMahasiswa(String npm).
     */
    public Mahasiswa getMahasiswa(String npm) {
        // Mengembalikan objek Mahasiswa dengan NPM tertentu dari daftar mahasiswa.
        // Jika tidak ada, kembalikan null atau lempar sebuah Exception.
        for (Mahasiswa mhs : this.mahasiswa) {
            if (mhs.getNpm().equals(npm)) {
                return mhs;
            } 
        }
        return null;
    }

    /**
     *rekap().
     */
    public String rekap() {
        // Mengembalikan hasil rekapan data mahasiswa yang dimiliki AsDos
        String rekapMhs = "";
        rekapMhs += "~ ~ ~ ~ ~ ~ ~ \n\n";
        for (Mahasiswa mhs : this.mahasiswa) {
            rekapMhs += (mhs.getNpm() + " - " + mhs.getNama() + "\n");
            rekapMhs += (mhs.rekap() + "\n");
        }
        return rekapMhs;
    }

    public String toString() {
        return (this.kode + " - " + this.nama);
    }
}

class SortByNPM implements Comparator<Mahasiswa> 
// Menmbandingkan dengan Comparator
{ 
    public int compare(Mahasiswa a, Mahasiswa b) 
    { 
        return a.compareTo(b);
    }
} 
