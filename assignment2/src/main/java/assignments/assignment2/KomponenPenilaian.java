package assignments.assignment2;

public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    /**
     * KomponenPenilaian.
     */
    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        // Constructor untuk KomponenPenilaian.
        // BanyakButirPenilaian digunakan untuk menentukan panjang butirPenilaian saja
        // (tanpa membuat objek-objek ButirPenilaian-nya).
        this.nama = nama;
        this.bobot = bobot;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        // Memasukkan butir ke butirPenilaian pada index ke-idx.
        butirPenilaian[idx] = butir;
    }

    public String getNama() {
        // Mengembalikan nama KomponenPenilaian.
        return this.nama;
    }

    /**
     * getRerata.
     */
    public double getRerata() {
        // Mengembalikan rata-rata butirPenilaian.
        double jumlahNilai = 0;
        int counter = 0;
        for (ButirPenilaian butir : this.butirPenilaian) {
            if (butir != null) {
                jumlahNilai += butir.getNilai();
                counter++;
            }
        }
        if (counter == 0) {
            return 0.0;
        }
        return jumlahNilai / counter;
    }

    public double getNilai() {
        // Mengembalikan rerata yang sudah dikalikan dengan bobot.
        return this.getRerata() * bobot / 100.00;
    }

    public String getDetail() {
        // Mengembalikan detail KomponenPenilaian sesuai permintaan soal.
        int hitung = 0;
        for (ButirPenilaian butir : this.butirPenilaian) {
            if (butir != null) {
                hitung++;
            }
        }
        // Membuat tampilan dari detail
        String hasil = "~~~ " + this.nama + " (" + Integer.toString(this.bobot) + "%) ~~~\n"; 
        int index = 0;
        for (ButirPenilaian butir : this.butirPenilaian) {
            index++;
            if (butir != null & hitung > 1) {
                hasil += (this.nama + " " + index + ": " + butir + "\n");
        } else if (butir != null & hitung == 1) {
                hasil += (this.nama +" : " + butir  + "\n");
            }
       }
       if (butirPenilaian.length > 1) {
           hasil += ("Rerata: " + String.format("%.2f", this.getRerata()) + "\n");
       }
           hasil += ("Kontribusi nilai akhir: " + String.format("%.2f",this.getRerata() * this.bobot / 100) + "\n\n");
     
    return hasil;
    }
    
    public int getBobot() {
        // Mengembalikan bobot penilaian
        return this.bobot;
    }

    @Override
    public String toString() {
        // Mengembalikan representasi String sebuah KomponenPenilaian sesuai permintaan soal.
        return "Rerata " + this.nama + ": " + String.format("%.2f", this.getRerata());
    }

}
