package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    /**
     * Mahasiswa.
     */
    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        // TODO: buat constructor untuk Mahasiswa.
        // Note: komponenPenilaian merupakan skema penilaian yang didapat dari GlasDOS.
        this.npm = npm;
        this.nama = nama;
        this.komponenPenilaian = komponenPenilaian;
    }

    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        // TODO: kembalikan KomponenPenilaian yang bernama namaKomponen.
        // Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.
        for (KomponenPenilaian komponen : this.komponenPenilaian) {
            if (komponen.getNama().equals(namaKomponen)) {
                return komponen;
            }
        }
        return null;
    }

    public String getNpm() {
        // TODO: kembalikan NPM mahasiswa.
        return this.npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A" :
            nilai >= 80 ? "A-" :
            nilai >= 75 ? "B+" :
            nilai >= 70 ? "B" :
            nilai >= 65 ? "B-" :
            nilai >= 60 ? "C+" :
            nilai >= 55 ? "C" :
            nilai >= 40 ? "D" : "E";
    }

    public String getNama() {
        return this.nama;
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    public String rekap() {
        // TODO: kembalikan rekapan sesuai dengan permintaan soal.
        String rekap = "";
        double nilai_akhir = 0;
        for (KomponenPenilaian nama_rekap : this.komponenPenilaian) {
            rekap += ("Rerata " + nama_rekap.getNama()+ ": " + String.format("%.2f",nama_rekap.getRerata()) + "\n");
            nilai_akhir += nama_rekap.getNilai();
        }
        
        rekap += ("Nilai akhir: " + String.format("%.2f", nilai_akhir) + "\n");
        rekap += ("Huruf: " + Mahasiswa.getHuruf(nilai_akhir) + "\n");
        rekap += (Mahasiswa.getKelulusan(nilai_akhir) + "\n");
        return rekap;
    } 

    public String toString() {
        // TODO: kembalikan representasi String dari Mahasiswa sesuai permintaan soal.
        return this.npm + " - " + this.nama;
    }

    /**
     * getDetail.
     */
    public String getDetail() {
        // TODO: kembalikan detail dari Mahasiswa sesuai permintaan soal.
        double nilai = 0;
        String detailMahasiswa = "";
        for (KomponenPenilaian komponen : this.komponenPenilaian) {
        	detailMahasiswa += komponen.getDetail();
            nilai += komponen.getNilai();
        }
        
        detailMahasiswa += ("Nilai akhir:" + String.format("%.2f", nilai) + "\n");
        detailMahasiswa += ("Huruf: " + Mahasiswa.getHuruf(nilai) + "\n");
        detailMahasiswa += Mahasiswa.getKelulusan(nilai);
        return detailMahasiswa;
    }

    @Override
    public int compareTo(Mahasiswa other) {
        // TODO: definisikan cara membandingkan seorang mahasiswa dengan mahasiswa lainnya.
        // Hint: bandingkan NPM-nya, String juga punya method compareTo.
        return this.npm.compareTo(other.npm);
    }
}
