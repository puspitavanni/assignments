package assignments.assignment2;

public class ButirPenilaian {
    // private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;

    /**
     * Butir penilaian.
     */
    public ButirPenilaian(double nilai, boolean terlambat) {
        // Constructor untuk ButirPenilaian.
        if (nilai < 0) {
            this.nilai = 0;
        } else {
            this.nilai = nilai;
        }
        this.terlambat = terlambat;
    }

    /**
     * getNilai().
     */
    public double getNilai() {
        // Mengembalikan nilai yang sudah disesuaikan dengan keterlambatan.
        if (this.terlambat == true) {
            return (this.nilai * 80 / 100.00);
        } else {
            return this.nilai;
        }
    }

    @Override
    public String toString() {
        // Mengembalikan representasi String dari ButirPenilaian sesuai permintaan soal.
        if (this.terlambat == true) {
            return (String.format("%.2f", this.getNilai()) + " (T)");
        } else {
            return (String.format("%.2f", this.getNilai()));
        }
    }
}
