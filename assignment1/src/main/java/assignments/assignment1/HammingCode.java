package assignments.assignment1;

/* PUSPITA SARI MATOVANNI
 * 1906299080
 * DDP2-F
 * KODE ASDOS BK 
 */

import java.lang.Math;
import java.util.Scanner;

public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;
    
    /**
     * INI FUNGSI ENCODE. 
     */
    public static String encode(String data) {
        //mendefinisikan setiap variabel yang dibuat
        int panjangData = data.length();
        int r = 0;
        int m = panjangData;
        //mencari r
        while (Math.pow(2, r) < m + r + 1) {
            r++;
        } 
        
        //MEMBUAT TEMPLATE HAMMING CODE
        
        //membuat string hamming berisi "0" semua sebagai default
        String hamming = "";
        for (int i = 1; i <= m + r + 1; i++) {
            hamming += "0";
        }
        
        /*Menyisipkan parity bit pada hamming code (menambahkan satu karakter pada indeks 0, 
         jadi akan ada m+r+1+(1) bit di string hamming)*/
        for (int i = 1; i < m + r + 1; i *= 2) {
            hamming = hamming.substring(0, i) + "1" + hamming.substring(i + 1, hamming.length());
        }
        
        //Menyalin data ke dalam hamming code
        /*(menyalin data satu per satu ke indeks yang tidak ditempati oleh parity bit
         * setelah itu slicing pada ata setiap kali indeks pertama telah tersalin)*/
        for (int i = 1; i < m + r + 1; i++) {
            if (hamming.charAt(i) == '0') {
                hamming = hamming.substring(0, i) + data.substring(0, 1) + hamming.substring(i + 1);
                data = data.substring(1);
            }
        }
        
        //MENGEVALUASI HAMMING CODE
        
        for (int i = 1; i < m + r + 1; i *= 2) {
            int jumlahSatu = 0;
            for (int j = i; j <= m + r; j++) {
                /*pada hamming code, jika bit pada redundant dilakukan '&' dengan bit yang lain 
                * dan menghasilkan bit redundant itu sendiri, maka bit tersebut merupakan
                * posisi pada  jumlahan bit redundant (MENCARI POSISI JUMLAHAN BIT REDUNDANT)
                */
                if ((i & j) == i) {
                    /*mengitung jumlah satu yang ada pada posisi tersebut*/
                    if (hamming.charAt(j) == '1') {
                        jumlahSatu++;
                    }
                }
            }
            /*mengganti bit yang salah jika jumlahan satu yang ada pada posisi redundant tersebut
            * ganjil, maka angka "1" yang berlebih atau berkurang akan 
            * diganti atau ditambah dengan "0" */

            if (jumlahSatu % 2 == 1) {
                hamming = hamming.substring(0, i) + "0" + hamming.substring(i + 1);
            } 
        }
        //MEMBUANG INDEKS PERTAMA PADA STRING HAMMING CODE YANG TIDAK TERPAKAI
        hamming = hamming.substring(1);
        return hamming;
    }

    //======================================================================================//    
    /**
     * INI FUNGSI DECODE.
     */
    public static String decode(String code) {

        //MENGHITUNG POSISI ERROR PADA HAMMING CODE

        /*jika bit redundant dilakukan operasi '&' dengan bit lainnya dan menghasilkan 
        * bit redundant itu sendiri,
        * maka bit yang lainnya merupakan posisi jumlahan redundant, 
        * lalu mengecek apakah jumlah angka satu yang dhasilkan ganjil atau genap, jika ganjil maka 
        * error akan ditambah dengan posisi bit yang error
        */
        int m = code.length();
        int err = 0;
        for (int i = 1; i < m; i *= 2) {
            int jumlahSatu = 0;
            for (int j = i; j <= m; j++) {
                if ((i & j) == i) {
                    if (code.charAt(j - 1) == '1') {
                        jumlahSatu++;
                    }
                }
            }
            if (jumlahSatu % 2 == 1) {
                err += i;
            }
        }

        //MENGGANTI BIT ERROR YANG SESUAI
        if (err > 0) {
            if (code.charAt(err - 1) == '1') {
                code = code.substring(0, err - 1) + "0" + code.substring(err); 
            } else {
                code = code.substring(0, err - 1) + "1" + code.substring(err);
            }
        }
        //MEMBUANG PARITY BIT

        int r = 0;
        while ((int)Math.pow(2, r) < code.length()) {
            r++;
        }
        r--;
        r = (int)Math.pow(2, r);
        while (r > 0) {
            code = code.substring(0, r - 1) + code.substring(r);
            r /= 2;
        }
        return code;
    }

    /**
     * Main program for Hamming Code.
     * @param args unused
     */
    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}
